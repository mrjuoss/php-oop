<?php

class Frog extends Animal
{
  private $name;
  private $legs;
  private $cold_blooded;

  public function __construct($name, $legs = 4, $cold_blooded = true)
  {
    parent::__construct($name, $legs = 4, $cold_blooded = true);
    $this->name = $name;
    $this->legs = $legs;
    $this->cold_blooded = $cold_blooded;
  }

  public function jump()
  {
    echo "hop hop";
  }
}
