<?php

require "animal.php";
require "frog.php";
require "ape.php";

$sheep = new Animal("shaun");

echo $sheep->getName(); // "shaun"
echo "<br />";
echo $sheep->getLegs(); // 2
echo "<br />";
echo $sheep->getColdblooded(); // false

echo "<br />";

$sungokong = new Ape("kera sakti");
echo $sungokong->getName();
echo "<br />";
$sungokong->yell(); // "Auooo"
echo "<br />";
echo $sungokong->getLegs();
echo "<br />";

echo "<br />";

$kodok = new Frog("buduk");
echo $kodok->getName(); // "shaun"
echo "<br />";
// $kodok->setColdBlooded();
$kodok->jump(); // "hop hop"
echo "<br />";
echo $kodok->getLegs();
echo "<br />";
echo $kodok->getColdBlooded(); // false
