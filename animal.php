<?php

class Animal
{
  private $name;
  private $legs;
  private $cold_blooded;

  public function __construct($name, $legs = 2, $cold_blooded = false)
  {
    $this->name = $name;
    $this->legs = $legs;
    $this->cold_blooded = $cold_blooded;
  }

  public function getName()
  {
    return "Nama Hewan : " . $this->name;
  }

  public function getLegs()
  {
    return "Jumlah Kaki : " . $this->legs;
  }

  public function getColdBlooded()
  {
    return $this->cold_blooded;
  }
}
